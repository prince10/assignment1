//
//  ViewController.m
//  clview
//
//  Created by Prince on 01/10/15.
//  Copyright (c) 2015 Prince. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
{
    NSArray *data;
}
@property (strong, nonatomic) IBOutlet UIButton *button1;
@property (strong, nonatomic) IBOutlet UITableView *taleView1;

@end

@implementation ViewController
@synthesize taleView1;

- (void)viewDidLoad {
    
    data=[[NSArray alloc]init];
    data= @[@"prince",@"raghav",@"arjun"];
    taleView1.hidden=TRUE;
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}


-(IBAction)buttonPressed{
    taleView1.hidden=FALSE;
    [taleView1 reloadData];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return data.count;

}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell=[taleView1 dequeueReusableCellWithIdentifier:@"id"];
    cell.textLabel.text=data[indexPath.row];
    return cell;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
